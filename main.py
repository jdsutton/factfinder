import argparse
import docx
from factfinder.PDF2TXT import PDF2TXT
import os
from pathlib import Path
import re
import sys

class FactFinder:
    _CACHE_DIR = '.factfinder'

    @classmethod
    def _getCachedFile(cls, path):
        directory = Path(path).parent.absolute()

        fName = os.path.basename(path)

        if not fName.endswith('.txt'):
            fName += '.txt'
        
        return os.path.join(str(directory), cls._CACHE_DIR, fName)

    @classmethod
    def getCachedText(cls, path):
        cached = cls._getCachedFile(path)

        try:
            with open(cached, 'r') as f:
                return f.read()
        except Exception as e:
            pass

        return None

    @classmethod
    def cache(cls, path, text):
        cached = cls._getCachedFile(path)

        Path(cached).parent.mkdir(parents=True, exist_ok=True)

        with open(cached, 'w') as f:
            f.write(text)

class Pattern:
    DOLLAR_AMOUNT = r'(\$[0-9]+(.[0-9]+)?)'
    DATE = r'((Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)\s+(\d{1,2}),?\s+(\d{4}))'
    ADDRESS = r'((\d{1,}) [a-zA-Z0-9\s]+(\,)? [a-zA-Z]+(\,)? [A-Z]{2} [0-9]{5,6})'

def getText(path):
    
    # Docx.
    if path.endswith('.docx'):
        doc = docx.Document(path)
        fullText = []
        
        for p in doc.paragraphs:
            fullText.append(p.text)
        
        return '\n'.join(fullText)

    # PDF.
    if path.endswith('.pdf'):

        # If cached text exists, return it.
        cached = FactFinder.getCachedText(path)

        if cached:
            return cached

        text = PDF2TXT.convert(path)
        
        FactFinder.cache(path, text)

        return text

    return ''

def listFiles(dir):
    paths = Path(dir).rglob('*')

    paths = map(str, paths)
    paths = filter(lambda x: '.factfinder' not in x, paths)

    return list(paths)

def findAll(pattern, text):
    result = [
        ' '.join(group[0].split())
        for group in re.findall(pattern, text)
        if len(group) > 0]

    return list(set(result))

def auto(path):
    files = listFiles(path)

    for file in files:

        text = getText(file)

        dollarAmounts = findAll(Pattern.DOLLAR_AMOUNT, text)
        dates = findAll(Pattern.DATE, text)
        addresses = findAll(Pattern.ADDRESS, text)

        display = len(dollarAmounts) > 0 \
            or len(dates) > 0 \
            or len(addresses) > 0

        if display:
            print('\033[1;34m' + os.path.basename(file) + '\033[0m')

            if len(dollarAmounts) > 0:
                print('Amounts: ' + str(dollarAmounts))

            if len(dates) > 0:
                print('Dates: ' + str(dates))

            if len(addresses) > 0:
                print('Addresses: ' + str(addresses))

            print('')

def search(path, query, chars):
    files = listFiles(path)

    query = query.lower()

    for file in files:

        print('\033[1;34m' + os.path.basename(file) + '\033[0m')

        text = getText(file)
        findText = text.lower()
        start = 0

        while True:
            start = findText.find(query, start)

            if start == -1: break

            matchStart = max(start - chars, 0)
            matchEnd = min(start + len(query) + chars, len(text))

            match = text[matchStart:start] \
                + '\033[0;32m' + text[start:start+len(query)] \
                + '\033[0m' + text[start+len(query):matchEnd]

            match = ' '.join(match.split())

            start += len(query)

            print('\n' + match)

if __name__ == '__main__':
        
    parser = argparse.ArgumentParser(
        prog='FactFinder by John D. Sutton',
        description='Analyze a directory and extract information',
    )

    parser.add_argument('mode', choices=['auto', 'search'], help='The mode of the progrram')
    parser.add_argument('directory', help='the directory to analyze')
    parser.add_argument('query', nargs='?', default='', help='the search query')
    parser.add_argument('chars', nargs='?', default=50, help='the # of characters around a result to display')

    args = parser.parse_args()

    if args.mode == 'auto':
        auto(args.directory)
    elif args.mode == 'search':
        search(args.directory, args.query, int(args.chars))
    else:
        print('Illegal mode: ' + args.mode)