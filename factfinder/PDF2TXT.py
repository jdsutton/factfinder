import cv2
import numpy as np
from pdf2image import convert_from_path
import pytesseract

class PDF2TXT:
    '''
    Source: https://medium.com/@dr.booma19/extracting-text-from-pdf-files-using-ocr-a-step-by-step-guide-with-python-code-becf221529ef
    '''

    @staticmethod
    def _deskew(image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.bitwise_not(gray)
        coords = np.column_stack(np.where(gray > 0))
        angle = cv2.minAreaRect(coords)[-1]
        
        if angle < -45:
            angle = -(90 + angle)
        else:
            angle = -angle

        (h, w) = image.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

        return rotated

    @classmethod
    def convert(cls, path):
        # Convert to image.
        pages = convert_from_path(path)

        extractedText = []

        for page in pages:
            # Preprocess the image.
            image = cls._deskew(np.array(page))

            # Extract text using OCR.
            text = pytesseract.image_to_string(image)
            extractedText.append(text)

        return '\n'.join(extractedText)
